from django.db import models

class Project(models.Model):
	pass

class Feedback(models.Model):
	pass

class Screenshot(models.Model):
	feedback_id = models.ForeignKey(Feedback, on_delete=models.CASCADE)
	url = models.URLField(max_length=500)
	resolution = models.CharField(max_length=20)
	file = models.FileField(upload_to='uploads/')

class Comment(models.Model):
	notify_reporter = models.BooleanField()
	is_public = models.BooleanField()
	anon_email = models.EmailField(max_length=100)
	anon_name = models.CharField(max_length=100)
	comment = models.CharField(max_length=100)
	time = models.TimeField(auto_now=True, auto_now_add=True)

	class Meta:
		abstract = True

class ScreenshotComment(Comment): 
	screenshot_id = models.ForeignKey(Screenshot, on_delete=models.CASCADE)
	is_resolved = models.BooleanField()

class FeedbackComment(Comment):
	feedback_id = models.ForeignKey(Feedback, on_delete=models.CASCADE)

class Attachment(models.Model):
	comment_id = models.ForeignKey(Comment, on_delete=models.CASCADE)
	file = models.FileField(upload_to='uploads/')		

class Workflow(models.Model):
	project_id = models.ForeignKey(Project, on_delete=models.CASCADE)
	name = models.CharField(max_length=100)
	ordering = models.CharField(max_length=50)
	is_default = models.BooleanField()		