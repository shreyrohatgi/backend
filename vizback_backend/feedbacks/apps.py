from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    name = "vizback_backend.feedbacks"
    verbose_name = _("Feedbacks")

    def ready(self):
        try:
            import vizback_backend.feedbacks.signals  # noqa F401
        except ImportError:
            pass
