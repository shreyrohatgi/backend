Vizback Backend
===============

Backend API for Vizback - Visual Feedback Tool

.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Built with Cookiecutter Django
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
     :target: https://github.com/ambv/black
     :alt: Black code style


Settings
--------

Moved to settings_.

.. _settings: http://cookiecutter-django.readthedocs.io/en/latest/settings.html

Basic Commands
--------------

Development
^^^^^^^^^^^^

* Activate Virtual Environment
* Install PostgreSQL on your system
* Install PGAdmin on your system
* Create `.env` file in root folder.
* Set enviroment variable to use `.env` file using `export DJANGO_READ_DOT_ENV_FILE=1;`
* Follow https://cookiecutter-django.readthedocs.io/en/latest/settings.html and set DATABASE_URL and credentials
* Install dependencies using::

    $ pip install -r requirements/local.txt

* run ::

    $ python manage.py migrate

    $ python manage.py runserver

Setting Up Your Users
^^^^^^^^^^^^^^^^^^^^^


* To create an **superuser account**, use this command::

    $ python manage.py createsuperuser

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.

Type checks
^^^^^^^^^^^

Running type checks with mypy:

::

  $ mypy vizback_backend

Test coverage
^^^^^^^^^^^^^

To run the tests, check your test coverage, and generate an HTML coverage report::

    $ coverage run -m pytest
    $ coverage html
    $ open htmlcov/index.html

Running tests with py.test
~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  $ pytest

Live reloading and Sass CSS compilation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Moved to `Live reloading and SASS compilation`_.

.. _`Live reloading and SASS compilation`: http://cookiecutter-django.readthedocs.io/en/latest/live-reloading-and-sass-compilation.html





Deployment
----------

The following details how to deploy this application.
